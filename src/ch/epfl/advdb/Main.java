/* This class is taken from ${HADOOP_HOME}/hadoop/src/examples/org/apache/hadoop/examples/WordCount.
	In contrast to the MapReduce tutorial WordCount code, this class use NonDeprecated classes 
		(i.e. insead of mapred, mapreduce package is used). ND in name refers to NonDeprecated.
 */
package ch.epfl.advdb;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;

import ch.epfl.advdb.keys.URefineKey;
import ch.epfl.advdb.keys.VRefineKey;
import ch.epfl.advdb.util.Comparators;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.Comparators.URefinePartitioner;
import ch.epfl.advdb.util.Comparators.URefinementComparator;
import ch.epfl.advdb.util.Comparators.URefinementGroupComparator;
import ch.epfl.advdb.util.Comparators.VRefinePartitioner;
import ch.epfl.advdb.util.Comparators.VRefinementComparator;
import ch.epfl.advdb.util.Comparators.VRefinementGroupComparator;
import ch.epfl.advdb.util.Constants.MyCounters;
import ch.epfl.advdb.values.LightValue;
import ch.epfl.advdb.values.MatrixField;

//TODO remove arguments for local execution before submitting
//TODO remove output redirection
public class Main {


	public static void main(String[] args) throws Exception {

		//On purpose allowing one more implicit argument
		if (args.length < 2 || args.length > 3) {
			System.err.println("Usage: netflix.jar <in> <out>");
			System.exit(2);
		}

		int remoteExec = 1;

		if(args.length == 3)
		{
			remoteExec = Integer.parseInt(args[2]);//Extra arg if I need to test locally
		}
		String homePath = "";
		if(remoteExec == 0)
		{
			homePath = "/home/manolee/";
		}
		else
		{
			homePath = "/std69/";

		}

//		PrintStream ps;
//		//Logging = not used in delivered code
//		if(remoteExec == 0)
//		{
//			ps = new PrintStream(homePath+"hadoopStuff/logs/out.log");
//		}
//		else
//		{
//			ps = new PrintStream("/export/home/std69/hadoopStuff/logs/out.log");
//
//		}
//		System.setOut(ps);


		final long startTime = System.currentTimeMillis();

		Configuration conf = new Configuration();


		FileSystem fs = FileSystem.get(conf);
		conf.setInt("mapred.reduce.tasks", Constants.reducersNo);
		conf.set("mapred.textoutputformat.separator", ",");

		/**
		 * Normalizing - row-wise
		 */

		Path inDir = new Path(args[0]);
		Path outDir = new Path(args[1]);

		/**
		 * Cleanup prior to new execution
		 */
		if (fs.exists(outDir)) {
			fs.delete(outDir, true);
		}

		// ...and then make sure it exists again
		//XXX only local use!
		if(remoteExec == 0)
		{
			if (fs.exists(inDir)) {
				fs.delete(inDir, true);
			}
			fs.mkdirs(inDir);	
		}

		//Useful for custom executions		
//						if(remoteExec == 1)
//						{
//							//fs.copyFromLocalFile(false, true, new Path("/export/home/std69/hadoopStuff/data/input/ratings_5000x100.dat"),inDir);
//							//			fs.copyFromLocalFile(false, true, new Path("/export/home/std69/hadoopStuff/data/input/BookActual.txt"),inDir);
//				
//							inDir = new Path("/netflix/input/large/");
//						}
//						else
//						{
//							fs.copyFromLocalFile(false, true, new Path(homePath+"hadoopStuff/data/input/ratings_5000x100.dat"),inDir);
//		//								fs.copyFromLocalFile(false, true, new Path(homePath+"hadoopStuff/data/input/BookActual.txt"),inDir);
//				
//						}

		Job rowNormalizing = new Job(conf, "normalize - rowwise");
		rowNormalizing.setJarByClass(Main.class);
		rowNormalizing.setMapperClass(ch.epfl.advdb.mappers.RowNormalizerMapper.class);
		rowNormalizing.setReducerClass(ch.epfl.advdb.reducers.RowNormalizerReducer.class);
		rowNormalizing.setOutputKeyClass(Text.class);
		rowNormalizing.setOutputValueClass(MatrixField.class);

		FileInputFormat.addInputPath(rowNormalizing, inDir);
		FileOutputFormat.setOutputPath(rowNormalizing, new Path(args[1]+"/Normalized"));
		final long normTimeStart = System.currentTimeMillis();
		rowNormalizing.waitForCompletion(true);
		final long normTimeEnd = System.currentTimeMillis();

		//Will be used in RMSE calculation
		long elemCount = rowNormalizing.getCounters().findCounter(MyCounters.M_NONBLANK_ENTRIES).getValue();

		//System.out.println("Total normalization time: " + (normTimeEnd - normTimeStart)/1000.0 + " sec" );

		/**
		 * Generating U + V
		 */
		//XXX careful! must provide input from a place accessible even during the automatic evaluation
		Path mockGeneration = new Path(args[1]+"/mockInput");

		if(remoteExec == 0)
		{
			fs.copyFromLocalFile(false, true, new Path(homePath+"hadoopStuff/data/input/mockGeneration.txt"),mockGeneration);
		}
		else
		{
			fs.copyFromLocalFile(false, true, new Path("/export/home/std69/hadoopStuff/data/input/mockGeneration.txt"),mockGeneration);
		}

		Job uGeneration = new Job(conf, "generate U");
		uGeneration.setJarByClass(Main.class);
		uGeneration.setMapperClass(ch.epfl.advdb.mappers.UGeneratorMapper.class);
		uGeneration.setReducerClass(ch.epfl.advdb.reducers.UGeneratorReducer.class);
		uGeneration.setOutputKeyClass(Text.class);
		uGeneration.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(uGeneration, /*inDir*/mockGeneration);
		FileOutputFormat.setOutputPath(uGeneration, new Path(args[1]+"/U_0"));

		final long genTimeStart = System.currentTimeMillis();
		uGeneration.waitForCompletion(true);

		Job vGeneration = new Job(conf, "generate V");
		vGeneration.setJarByClass(Main.class);
		vGeneration.setMapperClass(ch.epfl.advdb.mappers.VGeneratorMapper.class);
		vGeneration.setReducerClass(ch.epfl.advdb.reducers.VGeneratorReducer.class);
		vGeneration.setOutputKeyClass(Text.class);
		vGeneration.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(vGeneration, /*inDir*/mockGeneration);
		FileOutputFormat.setOutputPath(vGeneration, new Path(args[1]+"/V_0"));

		vGeneration.waitForCompletion(true);
		final long genTimeEnd = System.currentTimeMillis();
		System.out.println("Total generation time: " + (genTimeEnd - genTimeStart)/1000.0 + " sec" );
		/**
		 * Preparing chain
		 */
		Path normalizedM = new Path(args[1]+"/Normalized");

		//Not calculating at first loop =>  Bogus large value
//		double RMSE = 10E6;//Very large value on purpose
//		double RMSEprev = 0; //Used to have terminating condition based on delta
//
//		List<Double> rmseValues = new ArrayList<Double>();



		conf.setInt("halfway",1);
		//conf.setInt("firstV",1);

		//Chain: V -> U!
		//i loops through complete iterations
		for(int i = 0; i < Constants.iterations; i++) 
		{
			System.out.println("Refinement loop "+(i+1));
			Path uPath = new Path(args[1] + "/U_"+(i));
			Path vPath = new Path(args[1] + "/V_"+(i));

			final long loopStart = System.currentTimeMillis();

			//V REFINEMENT
			Job refineV = new Job(conf, "refine V");
			refineV.setJarByClass(Main.class);
			refineV.setMapperClass(ch.epfl.advdb.mappers.VRefinerMapper.class);
			refineV.setReducerClass(ch.epfl.advdb.reducers.VRefinerReducer.class);
			refineV.setOutputKeyClass(VRefineKey.class);
			refineV.setOutputValueClass(LightValue.class);
			refineV.setPartitionerClass(VRefinePartitioner.class);
			refineV.setSortComparatorClass(VRefinementComparator.class);
			refineV.setGroupingComparatorClass(VRefinementGroupComparator.class);

			FileInputFormat.addInputPath(refineV,uPath);
			FileInputFormat.addInputPath(refineV,vPath);
			FileInputFormat.addInputPath(refineV,normalizedM);

			Path outputRefinements = new Path(args[1]+"/V_"+(i+1));
			FileOutputFormat.setOutputPath(refineV, outputRefinements );
			if (fs.exists(outputRefinements)) {
				fs.delete(outputRefinements, true);
			}

			final long vStart = System.currentTimeMillis();
			refineV.waitForCompletion(true);
			final long vEnd = System.currentTimeMillis();
//			System.out.println("vRef took "+(vEnd - vStart) / 1000+" sec");

			//U REFINEMENT
			Job refineU = new Job(conf, "refine U");
			refineU.setJarByClass(Main.class);
			refineU.setMapperClass(ch.epfl.advdb.mappers.URefinerMapper.class);
			refineU.setReducerClass(ch.epfl.advdb.reducers.URefinerReducer.class);
			refineU.setOutputKeyClass(URefineKey.class);
			refineU.setOutputValueClass(LightValue.class);
			refineU.setPartitionerClass(URefinePartitioner.class);
			refineU.setSortComparatorClass(URefinementComparator.class);
			refineU.setGroupingComparatorClass(URefinementGroupComparator.class);

			vPath = outputRefinements;
			FileInputFormat.addInputPath(refineU,uPath);
			FileInputFormat.addInputPath(refineU,vPath);
			FileInputFormat.addInputPath(refineU,normalizedM);

			outputRefinements = new Path(args[1]+"/U_"+(i+1));
			FileOutputFormat.setOutputPath(refineU, outputRefinements );
			if (fs.exists(outputRefinements)) {
				fs.delete(outputRefinements, true);
			}

			final long uStart = System.currentTimeMillis();
			refineU.waitForCompletion(true);
			final long uEnd = System.currentTimeMillis();
//			System.out.println("uRef took "+(uEnd - uStart) / 1000+" sec");

			//Total times per iter.
			final long loopEnd = System.currentTimeMillis();
			System.out.println("Refinement Iteration "+(i+1)+" took "+(loopEnd - loopStart) / 1000+" sec");
			uPath = outputRefinements;

			//RMSE calculation based on values returned by counters
			//Not necessary to actually display it after all
//			Counters c = refineU.getCounters();
//			RMSE = 	Constants.getRMSE(c.findCounter(Constants.MyCounters.RMSE));
//			RMSE = Math.sqrt(RMSE / (float) elemCount);
//
//			rmseValues.add(RMSE);
//			System.out.println("Current RMSE: "+RMSE);
//			if(Math.abs(RMSE - RMSEprev) < 0.01)
//			{
//				System.out.println("No more significant improvements - exiting @ round "+(i+1));
//				break;
//			}
//			RMSEprev = RMSE;
		}

		final long endTime = System.currentTimeMillis();
		System.out.println("Total execution time: " + (endTime - startTime)/1000.0 );
//		for(double val : rmseValues)
//		{
//			System.out.println("RMSE: "+val);
//		}


	}


}
