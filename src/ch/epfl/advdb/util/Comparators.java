package ch.epfl.advdb.util;


import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Partitioner;

import ch.epfl.advdb.keys.URefineKey;
import ch.epfl.advdb.keys.VRefineKey;
import ch.epfl.advdb.util.Constants.matrixType;
import ch.epfl.advdb.values.LightValue;
import ch.epfl.advdb.values.MatrixField;

/**
 * Various comparators used whenever we introduce
 * a more 'complex' key, or we want to achieve some secondary sorting
 * @author manolee
 *
 */

public class Comparators {


	/**
	 * Partitioners
	 * @author manolee
	 *
	 */

	public static class URefinePartitioner extends Partitioner<URefineKey, LightValue>
	{
		public int getPartition (URefineKey key, LightValue value, int numPartitions) {

			int actualKey = key.getIndex(); //only take this part of the Key into account - load balance
			return actualKey % (numPartitions);
		}
	}

	public static class VRefinePartitioner extends Partitioner<VRefineKey, LightValue>
	{
		public int getPartition (VRefineKey key, LightValue value, int numPartitions) {

			int actualKey = key.getIndex(); //only take this part of the Key into account - load balance
			return actualKey % numPartitions;
		}
	}

/**
 * COMPARATORS
 */

	/**
	 * Refinement-specific comparators
	 * @author manolee
	 *
	 */

	//Just calls original compare method - used so that KeyComparator does not alter our 'buckets'
	public static class URefinementGroupComparator extends WritableComparator {
		protected URefinementGroupComparator() {
			super(URefineKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			URefineKey ip1 = (URefineKey) w1;
			URefineKey ip2 = (URefineKey) w2;
			return URefineKey.compare(ip1, ip2);

		}
	}

	//Key comparator for U - Used to dictate order of input values
	//The entire V matrix will arrive first.
	//Then, rows of M - U will arrive interleaved
	public static class URefinementComparator extends WritableComparator {
		protected URefinementComparator() {
			super(URefineKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			URefineKey ip1 = (URefineKey) w1;
			URefineKey ip2 = (URefineKey) w2;
			int cmp = URefineKey.compare(ip1, ip2);
			if(cmp != 0)
			{
				//i.e. not in same bucket / reducer -=> don't mess with them
				return cmp;
			}

			//V must come first
			if(ip1.getMatrixID() != ip2.getMatrixID())
			{
				if(ip1.getMtype() == matrixType.V)
				{
					return -1;
				}
				if(ip2.getMtype() == matrixType.V)
				{
					return 1;
				}
			}

			if(ip1.getRowNo() < ip2.getRowNo())
				return -1;
			else if (ip1.getRowNo() > ip2.getRowNo())
				return 1;

			cmp = (ip1.getMtype()).compareTo(ip2.getMtype());

			return cmp;

		}
	}

	
	/**
	 * Following methods are symmetric to the ones above
	 * @author manolee
	 *
	 */
	public static class VRefinementGroupComparator extends WritableComparator {
		protected VRefinementGroupComparator() {
			super(VRefineKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			VRefineKey ip1 = (VRefineKey) w1;
			VRefineKey ip2 = (VRefineKey) w2;
			return VRefineKey.compare(ip1, ip2);

		}
	}


	public static class VRefinementComparator extends WritableComparator {
		protected VRefinementComparator() {
			super(VRefineKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			VRefineKey ip1 = (VRefineKey) w1;
			VRefineKey ip2 = (VRefineKey) w2;
			int cmp = VRefineKey.compare(ip1, ip2);
			if(cmp != 0)
			{
				//i.e. not in same bucket / reducer
				return cmp;
			}

			if(ip1.getMatrixID() != ip2.getMatrixID())
			{
				//U must come first
				if(ip1.getMtype() == matrixType.U)
				{
					return -1;
				}
				if(ip2.getMtype() == matrixType.U)
				{
					return 1;
				}
			}

			if(ip1.getColNo() < ip2.getColNo())
				return -1;
			else if (ip1.getColNo() > ip2.getColNo())
				return 1;

			cmp = (ip1.getMtype()).compareTo(ip2.getMtype());

			return cmp;

		}
	}

}
