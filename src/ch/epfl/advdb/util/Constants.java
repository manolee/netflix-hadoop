package ch.epfl.advdb.util;

import org.apache.hadoop.mapreduce.Counter;


/**
 * Information used throughout the application,
 * such as matrix dimensions, chunk sizes, number of reducers etc.
 * @author manolee
 *
 */
public class Constants {

	public enum matrixType { M, V , U};

	public static int reducersNo = 88;//Number of reducers utilized @ cluster 

	public static int uBlock = reducersNo;//Used to specify chunks for refinement of U
	public static int vBlock = reducersNo/2;//Used to specify chunks for refinement of V



	//Typical presence, large number to make sure the iteration won't stop early
	public static int iterations = 100;

	//In-book example
//		public static int I = 5;
//		public static int K = 2;
//		public static int J = 5;

	//'Small' input dataset provided to us
//		public static int I = 5000;
//		public static int K = 10;
//		public static int J = 100;


	//Official input
	public static int I = 480189;
	public static int K = 10;
	public static int J = 17770;

	//Used during normalization
	public static int mRows = I;
	public static int mColumns = J;


	//Handy aliases
	public static int users = I;
	public static int movies = J;
	public static int dimensions = K;

	public static enum MyCounters { RMSE , M_NONBLANK_ENTRIES};


	//Used to store float values as long counters of Hadoop
	public static long setRMSE(float rmse)
	{
		//some precision will be lost, but not too significant
		return (long) (rmse * 10E8);
	}

	public static double getRMSE(Counter rmse)
	{
		//some precision will be lost, but not too significant
		return (rmse.getValue() / (double) 10E8);
	}

	public static long setSum(float sum)
	{
		//some precision will be lost, but not too significant
		return (long) (sum * 10E8);
	}

	public static double getSum(Counter sum)
	{
		//some precision will be lost, but not too significant
		return (sum.getValue() / (double) 10E8);
	}
}
