package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import ch.epfl.advdb.keys.URefineKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.LightValue;

/**
 * 
 * @author manolee
 * Class handling the map part of U refinement.
 * The entire U will be refined using this job.
 * 
 * Each reducer will receive a number of rows of U and M.
 * As for matrix V, it will be sent to all reducers
 */
public class URefinerMapper extends Mapper<Object, Text, URefineKey, LightValue> {

	URefineKey outputKey = new URefineKey();

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		StringTokenizer itr = new StringTokenizer(value.toString()," ,\t");

		if(!itr.hasMoreTokens()) return;

		String matrixID = itr.nextToken();

		int row = Integer.parseInt(itr.nextToken());
		int col = Integer.parseInt(itr.nextToken());
		//Grade
		float grade = Float.parseFloat(itr.nextToken());

		LightValue outputValue = new LightValue();
		outputValue.setGrade(grade);
		

		if(matrixID.equals("U"))
		{
			//Each row needs to reach separate reducer
			outputKey.setMatrixID('U');
			outputKey.setIndex(row  % Constants.uBlock);
			outputKey.setRowNo(row);
			
			//col to carry to refiner
			outputValue.setComplementary(col);
			
			context.write(outputKey,outputValue);
		}
		else if(matrixID.equals("V"))
		{
			//Sending to everyone
			outputKey.setMatrixID('V');

			outputValue.setComplementary(col);
			outputKey.setRowNo(row);
			for(int i = 0; i <  Constants.uBlock ; i++)
			{
				outputKey.setIndex(i);
				
				context.write(outputKey,outputValue);
			}

		}
		else if(matrixID.equals("M"))
		{
			//Each row block needs to reach separate reducer
			outputKey.setMatrixID('M');
			outputKey.setIndex(row  %  Constants.uBlock);
			outputKey.setRowNo(row);
			outputValue.setComplementary(col);
			context.write(outputKey,outputValue);
		}
	}
}