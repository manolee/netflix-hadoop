package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.MatrixField;

public class RowNormalizerMapper extends Mapper<Object, Text, Text, MatrixField> {


	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		StringBuffer buf = new StringBuffer();

		//UserID
		String uid = itr.nextToken();
		//MovieID
		String mid = itr.nextToken();
		//Grade
		String grade = itr.nextToken();
		//Also Date, but irrelevant

		//Statistics kept once to use for RMSE later on
		context.getCounter(Constants.MyCounters.M_NONBLANK_ENTRIES).increment(1);

		context.write(new Text(uid), new MatrixField('M', uid, mid, grade));


	}
}