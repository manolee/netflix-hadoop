package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

//import ch.epfl.advdb.MatrixField;
import ch.epfl.advdb.keys.VRefineKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.LightValue;


/**
 * 
 * @author manolee
 * Class handling the map part of V refinement.
 * The entire V will be refined using this job.
 * 
 * Each reducer will receive a number of rows of V and M.
 * As for matrix U, it will be sent to all reducers.
 * We will attempt to leverage this heavy traffic by 
 * performing the calculation of RMSE towards the end of the
 * subsequent reducing phase 
 */
public class VRefinerMapper extends Mapper<Object, Text, VRefineKey, LightValue> {

	VRefineKey outputKey = new VRefineKey();

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		StringTokenizer itr = new StringTokenizer(value.toString()," ,\t");

		if(!itr.hasMoreTokens()) return;

		String matrixID = itr.nextToken();

		
		int row = Integer.parseInt(itr.nextToken());
		int col = Integer.parseInt(itr.nextToken());
		//Grade
		float grade = Float.parseFloat(itr.nextToken());

		LightValue outputValue = new LightValue();
		outputValue.setGrade(grade);

		if(matrixID.equals("V"))
		{
			//Each row needs to reach separate reducer
			outputKey.setMatrixID('V');
			outputKey.setIndex(col % Constants.vBlock);
			outputKey.setColNo(col);
			
			//row to carry to refiner
			outputValue.setComplementary(row);

			context.write(outputKey,outputValue);
		}
		else if(matrixID.equals("U"))
		{
			//Sending to everyone
			outputKey.setMatrixID('U');

			outputValue.setComplementary(row);
			outputKey.setColNo(col);
			for(int i = 0; i < Constants.vBlock; i++)
			{
				outputKey.setIndex(i);
				
				context.write(outputKey,outputValue);
			}

		}
		else if(matrixID.equals("M"))
		{
			//Each row block needs to reach separate reducer
			outputKey.setMatrixID('M');
			outputKey.setIndex(col  % Constants.vBlock);
			outputKey.setColNo(col);
			outputValue.setComplementary(row);
			context.write(outputKey,outputValue);
		}
	}
}
