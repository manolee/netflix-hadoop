package ch.epfl.advdb.mappers;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import ch.epfl.advdb.util.Constants;

/**
 * 
 * @author manolee
 * Generating matrix V
 * To enable dynamic generation, this mapper accepts as input a single file with a single entry (i.e. we provide a dummy file to trigger the job.)
 * Then, it broadcasts to all reducers the 'command' to generate relevant rows of V
 */
public class VGeneratorMapper extends Mapper<Object, Text, Text, IntWritable> {

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		//System.out.println("Bcasting mock V");
		for(int i = 1; i <= Constants.J; i++)
			context.write(new Text(i+""), new IntWritable(1));

	}
}