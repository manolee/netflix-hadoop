package ch.epfl.advdb.values;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * 
 * @author manolee
 * Internal representation of a matrix field
 */

public class MatrixField implements Writable
{
	char matrixID; //'M', 'U', 'V', 'P'
	int row;
	int col;
	float grade;

	public MatrixField(char mtype, int row, int col, float grade) {

		this.matrixID = mtype;
		this.row = row;
		this.col = col;
		this.grade = grade;

	}

	public MatrixField(char mtype, String uid, String mid, String grade) {

		this.matrixID = mtype;
		this.row = Integer.parseInt(uid);
		this.col = Integer.parseInt(mid);
		this.grade = Float.parseFloat(grade);

	}

	public MatrixField() {}

	public float getGrade() {
		return grade;
	}



	public void setGrade(float grade) {
		this.grade = grade;
	}



	public char getMatrixID() {
		return matrixID;
	}



	public void setMatrixID(char matrixID) {
		this.matrixID = matrixID;
	}



	public int getRow() {
		return row;
	}



	public void setRow(int row) {
		this.row = row;
	}



	public int getCol() {
		return col;
	}



	public void setCol(int col) {
		this.col = col;
	}



	@Override
	public void readFields(DataInput in) throws IOException {
		matrixID = in.readChar();
		row = in.readInt();
		col = in.readInt();
		grade = in.readFloat();

	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeChar(matrixID);
		out.writeInt(row);
		out.writeInt(col);
		out.writeFloat(grade);
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		//buf.append(",");
		buf.append(row);
		buf.append(",");
		buf.append(col);
		buf.append(",");
		buf.append(grade);
		return buf.toString();

	}

}