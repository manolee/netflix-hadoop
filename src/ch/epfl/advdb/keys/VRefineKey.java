package ch.epfl.advdb.keys;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

import ch.epfl.advdb.util.Constants.matrixType;

/**
 * Key used during refinement of V
 * @author manolee
 *
 */
public class VRefineKey  implements WritableComparable {

	

	int index;
	
	//Secondary sort keys. Will be used to avoid buffering unneeded amounts of received values in memory of reducer
	char matrixID;
	matrixType mtype;
	int colNo;


	public char getMatrixID() {
		return matrixID;
	}

	public void setMatrixID(String matrixID) {
		this.matrixID = matrixID.charAt(0);
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type");
		}
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	
	
	public matrixType getMtype() {
		return mtype;
	}

	public void setMtype(matrixType mtype) {
		this.mtype = mtype;
	}

	public void setMatrixID(char matrixID) {
		this.matrixID = matrixID;
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type"); System.exit(-1);
		}
	}
	
	public int getColNo() {
		return colNo;
	}

	public void setColNo(int colNo) {
		this.colNo = colNo;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		index = in.readInt();
		matrixID = in.readChar();
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type"); System.exit(-1);
		}
		colNo = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(index);
		out.writeChar(matrixID);
		out.writeInt(colNo);
	}

	@Override
	public int compareTo(Object other) {
		VRefineKey o = (VRefineKey)other;
		if (this.getIndex() < o.getIndex()) {
			return -1;
		} else if (this.getIndex() > o.getIndex()) {
			return +1;
		}
		return 0;
	}

	public static int compare(VRefineKey ip1, VRefineKey ip2) {

		if (ip1.getIndex() < ip2.getIndex()) {
			return -1;
		} else if (ip1.getIndex() > ip2.getIndex()) {
			return +1;
		}


		return 0;
	} 



}