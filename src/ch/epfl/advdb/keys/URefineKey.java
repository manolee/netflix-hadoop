package ch.epfl.advdb.keys;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

import ch.epfl.advdb.util.Constants.matrixType;

/**
 * Key used during refinement of U
 * @author manolee
 *
 */
public class URefineKey  implements WritableComparable {

	int index;
	
	//Secondary sort keys. Will be used to avoid buffering unneeded amounts of received values in memory of reducer
	char matrixID;
	matrixType mtype;
	int rowNo;


	public char getMatrixID() {
		return matrixID;
	}

	public void setMatrixID(String matrixID) {
		this.matrixID = matrixID.charAt(0);
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type");
		}
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public matrixType getMtype() {
		return mtype;
	}

	public void setMtype(matrixType mtype) {
		this.mtype = mtype;
	}

	public void setMatrixID(char matrixID) {
		this.matrixID = matrixID;
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type"); System.exit(-1);
		}
	}
	
	public int getRowNo() {
		return rowNo;
	}

	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		index = in.readInt();
		matrixID = in.readChar();
		switch(this.matrixID)
		{
		case 'M': this.mtype = matrixType.M; break;
		case 'V': this.mtype = matrixType.V; break;
		case 'U': this.mtype = matrixType.U; break;
		default: System.err.println("Unknown matrix type"); System.exit(-1);
		}
		rowNo = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(index);
		out.writeChar(matrixID);
		out.writeInt(rowNo);
	}

	
	//Used/extended by comparators
	@Override
	public int compareTo(Object other) {
		URefineKey o = (URefineKey)other;
		if (this.getIndex() < o.getIndex()) {
			return -1;
		} else if (this.getIndex() > o.getIndex()) {
			return +1;
		}
		return 0;
	}

	public static int compare(URefineKey ip1, URefineKey ip2) {

		if (ip1.getIndex() < ip2.getIndex()) {
			return -1;
		} else if (ip1.getIndex() > ip2.getIndex()) {
			return +1;
		}


		return 0;
	} 



}
