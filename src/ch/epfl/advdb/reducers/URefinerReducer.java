package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.keys.URefineKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.LightValue;
import ch.epfl.advdb.values.MatrixField;

/**
 * Job that performs refinement on the entire U matrix.
 * In our processing chain, this job follows a V refinement.
 * 
 * We can therefore compute the new RMSE on the fly after U has been refined as well
 *  
 * @author manolee
 *
 */
public class URefinerReducer extends Reducer<URefineKey,LightValue,Text,MatrixField> {

	public void reduce(URefineKey key, Iterable<LightValue> values, Context context) throws IOException, InterruptedException {

		Iterator iter = values.iterator();

		float RMSE = 0;

		float V[][] = new float[Constants.dimensions][Constants.movies];	
		//Row-at-a-time
		float M[] = new float[Constants.movies];

		for(int i = 0; i < Constants.movies; i++)
		{
			M[i] = -256;
		}

		//Again, entire row
		float U[] = new float[Constants.dimensions];

		for(int i = 0; i < Constants.dimensions * Constants.movies; i++)
		{
			LightValue val = (LightValue) iter.next();

			int rowID = key.getRowNo();//val.getRow();
			int colID = val.getComplementary();//getCol();

			V[rowID - 1][colID - 1] = val.getGrade();
		}

		//This would make sense if U was refined first.
		//Still kept here in case of permutation tests
		int halfway = context.getConfiguration().getInt("halfway",-1);
		//context.getConfiguration().setInt("firstV",0);

		int r; //Row currently refined
		while(iter.hasNext())
		{
			halfway--;

			//Need to be careful now, I don't know how many Ms follow 
			for(int i = 0; i < Constants.movies; i++)
			{
				M[i] = -256;
			}
			LightValue valM = (LightValue) iter.next();
			r = key.getRowNo();//valM.getRow();

			while(key.getMatrixID() == 'M')
			{
				int colID = valM.getComplementary();//getCol();
				M[colID - 1] = valM.getGrade();
				valM = (LightValue) iter.next(); 
			}

			//At this point, valM will contain a U var
			int colID = valM.getComplementary();
			U[colID - 1] = valM.getGrade();

			//The rest d entries will concern U
			for(int i = 1; i < Constants.dimensions; i++)
			{
				LightValue val = (LightValue) iter.next();
				colID = val.getComplementary();//getCol();
				U[colID - 1] = val.getGrade();
			}

			//Performing calculations per-row - all columns of a row initially
			//"j for which Mrj non-blank
			int s = 0; //Column of row r to be refined
			while( s < Constants.dimensions)
			{
				float outerSum = 0;
				float denum = 0;
				for(int j = 0; j < Constants.movies; j++)
				{
					if(M[j] != -256)
					{
						float innerSum = 0;
						for(int k = 0; k < Constants.dimensions; k++)
						{
							//Inner sum
							if(k != s)
							{
								innerSum += (U[k] * V[k][j]);
							}
						}
						float diff = M[j] - innerSum;

						outerSum += (V[s][j] * diff);
						denum += (V[s][j] * V[s][j]);
					}
				}
				float x = outerSum / denum;

				MatrixField result = new MatrixField('U', r, s+1, x);
				context.write(new Text("U"), result);

				//Similar process for overfitting as in V, yet currently inactive
				if(halfway < 0)
				{
					U[s] = x;

				}
				else
				{
					//Attempt to avoid overfitting
					U[s] = (x + U[s]) / (float) 2;//"Meet halfway"
					context.getConfiguration().setInt("halfway",halfway);
					//					System.out.println("We're halfway there");
				}
				s++;
			}


			/**
			 * Now we can actually calculate a part of U*V and calculate RMSE on the fly! 
			 * Not needed to display it after all
			 */
			
//			for(int j = 0 ; j < Constants.movies; j++)
//			{
//				float p = 0;
//				for(int i = 0 ; i < Constants.dimensions; i++)
//				{
//					p += U[i] * V[i][j]; 
//				}
//				if(M[j] != -256)
//				{
//					float diff = (M[j] - p);
//					RMSE += (diff * diff);
//				}
//
//			}

		}
//		context.getCounter(Constants.MyCounters.RMSE).increment(Constants.setRMSE(RMSE));

	}
}
