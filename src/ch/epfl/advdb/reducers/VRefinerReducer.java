package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.keys.VRefineKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.LightValue;
import ch.epfl.advdb.values.MatrixField;

/**
 * Job that performs refinement on the entire V matrix.
 * Heaviest job in the application. Each reducer will initially receive the entire U matrix, and store it in an in-mem array.
 * Entries of M and V will then arrive interleaved , one column of each matrix at a time.
 * 
 * Note: In an attempt to handle overfitting, the 1st iteration of this part will only move the value of refined components 
 * halfway towards the dictated optimized value
 *  
 * @author manolee
 *
 */
public class VRefinerReducer extends Reducer<VRefineKey,LightValue,Text,MatrixField> {

	public void reduce(VRefineKey key, Iterable<LightValue> values, Context context) throws IOException, InterruptedException {
		Iterator iter = values.iterator();
	//	System.out.println("I am Reducing");
//		for(LightValue val: values)
//				{
//					System.out.println(key.getMatrixID()+val.toString());
////					if(key.getMatrixID() == 'M')
////					{
////						System.out.println("That's sth");
////					}
//				}
		float U[][] = new float[Constants.users][Constants.dimensions];	

		//Col-at-a-time
		float M[] = new float[Constants.users];

		//Fill with symbolic number to show that entry is non-existing
		for(int i = 0; i < Constants.users; i++)
		{
			M[i] = -256;
		}

		float V[] = new float[Constants.dimensions];

		for(int i = 0; i < Constants.dimensions * Constants.users; i++)
		{
			LightValue val = (LightValue) iter.next();

			int rowID = val.getComplementary();
			int colID = key.getColNo();
			U[rowID - 1][colID - 1] = val.getGrade();
		}

		int s; //Col currently refined

		int halfway = context.getConfiguration().getInt("halfway",-1);

		//int firstV = context.getConfiguration().getInt("firstV",-1);
		
		while(iter.hasNext())
		{
			int mcount = 0;
			halfway--;
			//Need to be careful now, I don't know explicitly how many Ms follow 
			for(int i = 0; i < Constants.users; i++)
			{
				M[i] = -256;
			}

			LightValue valM = (LightValue) iter.next();
			s = key.getColNo();
//			System.out.println("----");
			while(key.getMatrixID() == 'M')
			{
				int rowID = valM.getComplementary();
				M[rowID - 1] = valM.getGrade();
				valM = (LightValue) iter.next();
//				System.out.println("Valid value --> "+valM.getGrade());
				mcount++;
			}

			//At this point, valM will contain a V var
			int rowID = valM.getComplementary();//getRow();
			V[rowID - 1] = valM.getGrade();

			//The rest d entries will concern V
			for(int i = 1; i < Constants.dimensions; i++)
			{
				LightValue val = (LightValue) iter.next();
				rowID = val.getComplementary();//getRow();
				V[rowID - 1] = val.getGrade();
			}

			//Performing calculations per-row - all columns of a row initially
			int r = 0; //Num of row currently refined
			while( r < Constants.dimensions)
			{

				float outerSum = 0;
				float denum = 0;
				//"i for which Mis non-blank
				for(int i = 0; i < Constants.users; i++)
				{
					if(M[i] != -256)
					{
						float innerSum = 0;
						for(int k = 0; k < Constants.dimensions; k++)
						{
							//Inner sum
							if(k != r)
							{
								innerSum += (U[i][k] * V[k]);
							}
						}
						float diff = M[i] - innerSum;

						outerSum += (U[i][r] * diff);
						denum += (U[i][r] * U[i][r]);
//						System.out.println("Contrib "+U[i][r]);
					}
				}
				float y = denum > 0 ? outerSum / denum : 0;
				MatrixField result = new MatrixField('V', r+1, s, y);
				context.write(new Text("V"), result);
				//System.out.println("Flushing out V"+result.toString());
				//Attempt to handle overfitting
				if(halfway < 0)
				{	
					V[r] = y;
				}
				else
				{
					V[r] = (y + V[r]) / (float) 2; //"Meet halfway"
					context.getConfiguration().setInt("halfway",halfway);
				}
				r++;
			}
		}
	}
}

