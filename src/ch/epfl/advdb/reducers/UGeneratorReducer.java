package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;


/**
 * 
 * @author manolee
 * Rationale: Initialize U with random values following a Gaussian distribution. Values range between (-1,1)
 */
public class UGeneratorReducer extends Reducer<Text,IntWritable,Text,Text> {

	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		StringBuffer buf = new StringBuffer();

		Random gen = new Random();

		for(int i = 0; i < Constants.dimensions; i++)
		{
			buf.setLength(0);
			//buf.append(",");
			buf.append(key);
			buf.append(",");
			buf.append(i+1);
			buf.append(",");

			double val = gen.nextGaussian();

			//						buf.append("1");//Used to perform tests with deterministic input
			buf.append(val % 1);

			context.write(new Text("U"), new Text(buf.toString()));
		}
	}
}