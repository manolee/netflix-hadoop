package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.MatrixField;

public class RowNormalizerReducer extends Reducer<Text,MatrixField,Text,Text> {

	public void reduce(Text key, Iterable<MatrixField> values, Context context) throws IOException, InterruptedException {
		float total = 0;
		long count = 0;
		List<MatrixField> secondPass = new ArrayList<MatrixField>(Constants.mRows);

		for (MatrixField val : values) {
			total += val.getGrade();
			count++;
			MatrixField copy = new MatrixField('M',val.getRow(),val.getCol(),val.getGrade());
			secondPass.add(copy);
		}
		
		total = total / (float) count; //avg
//		total = 0; //Used to test with original values
		
		StringBuffer buf = new StringBuffer();

		for(Object iter : secondPass)
		{
			MatrixField val = (MatrixField) iter; 
			
			float newGrade = val.getGrade() - total;
			val.setGrade(newGrade);
			String newKey = ""+val.getMatrixID();
			buf.setLength(0);
			//buf.append(",");
			buf.append(val.getRow());
			buf.append(",");
			buf.append(val.getCol());
			buf.append(",");
			buf.append(val.getGrade());
			context.write(new Text(newKey), new Text(buf.toString()));
		}
	}
}